class Memory:
    def __init__(self):
        self.flush()

    def add(self, image_s, agent_s, action, reward, value, policy, flag):
        self.image_s.append(image_s)    # array
        self.agent_s.append(agent_s)    # array
        self.action.append(action)      # int
        self.reward.append(reward)      # float
        self.value.append(value)        # float
        self.policy.append(policy)      # float
        self.flag.append(flag)          # bool
        return

    def flush(self):
        self.image_s = []
        self.agent_s = []
        self.action = []
        self.reward = []
        self.value = []
        self.policy = []
        self.flag = []
        return
