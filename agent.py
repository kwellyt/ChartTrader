import numpy as np
from builder import build
from memory import Memory
from rlsaber.util import compute_returns, compute_gae


class Agent:
    def __init__(self, cnn_size, i_shape=[50, 50, 3], a_shape=5, action_size=3,
                 time_horizon=128, batch_size=32, epochs=3, lr=0.0004, gamma=0.999, lam=0.95,
                 epsilon=0.1, grad_clip=0.5, value_factor=0.5, entropy_factor=0.01, scope='ppo'):
        self.image_shape = i_shape
        self.agent_shape = a_shape
        self.action_size = action_size
        self.cnn_size = cnn_size
        self.time_horizon = time_horizon
        self.batch_size = batch_size
        self.epochs = epochs
        self.learning_rate = lr
        self.gamma = gamma
        self.lam = lam
        self.epsilon = epsilon
        self.grad_clip = grad_clip
        self.value_factor = value_factor
        self.entropy_factor = entropy_factor
        self._act, self._train, self.show = build(cnn_size, action_size, lr, i_shape, a_shape,
                                                  epsilon, grad_clip, value_factor, entropy_factor)

        self.old_state = dict(agent_s=None, image_s=None, action=None, value=None, policy=None, flag=None)
        self.trajectory = Memory()
        self.timestep = 0
        return

    def act(self, agent, image, reward, flag, buy_signal, sell_signal, training=True):
        action, log_policy, value = self._act(agent, image)
        action = np.asscalar(action)
        value = np.asscalar(value)
        volume = np.asscalar(np.exp(log_policy))
        # validation
        if not buy_signal:
            if action == 1:
                action = 0
                reward = 0.0

        if not sell_signal:
            if action == 2:
                action = 0
                reward = 0.0

        if training:
            if self.old_state['image_s'] is not None:
                self.trajectory.add(reward=reward, **self.old_state)

            if self.timestep > 0 and self.timestep % self.time_horizon == 0:
                bootstrap_value = value
                loss = self.train(bootstrap_value)
                print('LOSS: %.4f' % loss)

            self.timestep += 1
            self.old_state['agent_s'] = agent
            self.old_state['image_s'] = image
            self.old_state['action'] = action
            self.old_state['value'] = value
            self.old_state['policy'] = log_policy
            self.old_state['flag'] = flag
        return action, volume

    def train(self, bootstrap_value):
        agents = np.concatenate(self.trajectory.agent_s)
        images = np.concatenate(self.trajectory.image_s)
        actions = np.array(self.trajectory.action).reshape(1, -1)
        rewards = np.array(self.trajectory.reward).reshape(1, -1)
        values = np.array(self.trajectory.value).reshape(1, -1)
        log_policies = np.array(self.trajectory.policy).reshape(1, -1)
        flags = np.array([1.0 if f else 0.0 for f in self.trajectory.flag]).reshape(1, -1)

        returns = compute_returns(rewards, bootstrap_value, flags, self.gamma)
        advantages = compute_gae(rewards, values, bootstrap_value, flags, self.gamma, self.lam)
        advantages = (advantages - np.mean(advantages)) / np.std(advantages)

        agents, images, actions, advantages, returns, log_policies = self.shuffle(agents, images, actions,
                                                                                  advantages, returns, log_policies)

        for epoch in range(self.epochs):
            batch_loss = []
            for i in range(int(self.time_horizon / self.batch_size)):
                batch_agents = self.batch(agents, i, True)
                batch_images = self.batch(images, i, True)
                batch_actions = self.batch(actions, i)
                batch_returns = self.batch(returns, i)
                batch_advantages = self.batch(advantages, i)
                batch_log_policies = self.batch(log_policies, i)
                loss, __ = self._train(batch_agents, batch_images, batch_actions, batch_returns,
                                       batch_advantages, batch_log_policies)
                batch_loss.append(loss)
        self.trajectory.flush()
        return np.mean(batch_loss)

    def shuffle(self, agents, images, actions, advantages, returns, log_policies):
        indices = np.random.permutation(range(self.time_horizon))
        agents = agents[indices]
        images = images[indices]
        actions = actions[:, indices]
        advantages = advantages[:, indices]
        returns = returns[:, indices]
        log_policies = log_policies[:, indices]
        return agents, images, actions, advantages, returns, log_policies

    def batch(self, data, i, state=False):
        if state:
            batch_data = data[(self.batch_size * i):(self.batch_size * (i + 1))]
        else:
            batch_data = data[:, (self.batch_size * i):(self.batch_size * (i + 1))]
            batch_data = np.reshape(batch_data, [-1])
        return batch_data
