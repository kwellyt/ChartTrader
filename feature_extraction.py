import pandas as pd
import technical_trading_index as tti
import datetime
import os


def get_visual_index(dfm):
    dfm = tti.BBANDS(dfm)
    dfm = tti.MACD(dfm)
    dfm = tti.SAR(dfm)
    dfm = tti.SMA(dfm)
    dfm = tti.RSI(dfm)
    dfm = tti.STOCH(dfm)
    dfm = tti.ADX_with_DI(dfm)
    dfm = tti.CCI(dfm)
    return dfm


def get_numeric_index(dfm):
    dfm = tti.ROC(dfm)
    dfm = dfm.drop(['open', 'high', 'low', 'close', 'volume', 'ROC_V'], axis=1)
    return dfm


def path_check(path):
    if os.path.exists(path) is False:
        os.mkdir(path)
    return


# market data
data = pd.read_csv('./data/6E_intr.csv', header=None)
data.columns = ['symbol', 'date', 'open', 'high', 'low', 'close', 'volume']
data = data.iloc[::-1]
vars = ['open', 'high', 'low', 'close']
for var in vars:
    data[var] = data[var] * 10000

data['date'] = [datetime.datetime.strptime(d, '%Y%m%d %H%M%S').strftime('%Y-%m-%d %H:%M:%S') for d in data['date']]
data['volume'] = data['volume'].diff().fillna(0)
data = data.drop(['symbol'], axis=1)
data.to_csv('./data/market_data.csv', index=False)
date_2016 = [i for i in data.index if data.loc[i, 'date'][:4] == '2016']
date_2017 = [i for i in data.index if data.loc[i, 'date'][:4] == '2017']
date_2018 = [i for i in data.index if data.loc[i, 'date'][:4] == '2018']
data.loc[date_2016, ['date', 'close']].to_pickle('./data/market_2016.pkl')
data.loc[date_2017, ['date', 'close']].to_pickle('./data/market_2017.pkl')
data.loc[date_2018, ['date', 'close']].to_pickle('./data/market_2018.pkl')

# visualization
path_check('./data/visualization')
data = pd.read_csv('./data/market_data.csv')
visual_data = get_visual_index(data).dropna()
visual_data = visual_data.set_index('date')
visual_data['date'] = range(len(visual_data))
visual_data.to_pickle('./data/visualization/data_with_visual_index.pkl')

# numeric
path_check('./data/numeric')
data = pd.read_csv('./data/market_data.csv')
numeric_data = get_numeric_index(data).dropna()
numeric_data = numeric_data.set_index('date')
numeric_data.to_pickle('./data/numeric/data_with_numeric_index.pkl')
