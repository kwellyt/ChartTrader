from agent import Agent
from environment import Market
from util import path_check
import argparse
import os
import tensorflow as tf


def train():
    parser = argparse.ArgumentParser()
    parser.add_argument('--i', nargs='+', type=str, default=['SMA'], help='Technical index')
    parser.add_argument('--y', nargs='+', type=int, default=['2016', '2017'], help='The period of used data')
    parser.add_argument('--c', type=int, default=32, help='CNN feature map size')
    parser.add_argument('--e', type=int, default=10, help='Total episodes')
    parser.add_argument('--h', type=int, default=128, help='Time horizon')
    parser.add_argument('--batch', type=int, default=32, help='batch size')
    args = parser.parse_args()

    folder_path = '%s%s/' % ('_'.join(args.i), args.c)
    model_path = os.path.join('./models/', folder_path)
    log_path = './logs/'
    path_check('./models')
    path_check(model_path)
    path_check(log_path)

    # train
    tf.reset_default_graph()
    with tf.Session() as sess:
        agent = Agent(args.c, time_horizon=args.h, batch_size=args.batch)
        envs = Market(args.i, args.y, args.h)
        sess.run(tf.global_variables_initializer())
        saver = tf.train.Saver()

        for i in range(1, args.e + 1, 1):
            date, state = envs.reset()
            reward = 0
            flag = False
            buy_signal = True
            sell_signal = False
            A_cnt = {0: 0,
                     1: 0,
                     2: 0}

            while True:
                action, volume = agent.act(state['agent'], state[args.i[0]], reward, flag,
                                           buy_signal, sell_signal, training=True)
                A_cnt[action] += 1
                try:
                    date, state, reward, flag, buy_signal, sell_signal = envs.step(action, volume, agent.timestep)
                except tf.errors.OutOfRangeError:
                    break

                if flag:
                    arg = (i, agent.timestep, A_cnt[1], A_cnt[2], A_cnt[0], envs.profit, reward)
                    print('%03d %06d BUY: %04d SELL: %04d HOLD: %04d PROFIT: %.5f REWARD: %.5f' % arg, end=' ')
                    A_cnt = {0: 0,
                             1: 0,
                             2: 0}
                else:
                    arg = (A_cnt[1], A_cnt[2], A_cnt[0], envs.profit, volume, action, buy_signal, sell_signal)
                    print('[ BUY: %04d SELL: %04d HOLD: %04d PROFIT: %.5f %.3f %d %s %s]     ' % arg, end='\r')
            saver.save(sess, model_path + 'model', global_step=i)
    return print('\n')


if __name__ == '__main__':
    train()
