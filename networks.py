import tensorflow as tf


class Networks:
    def __init__(self, image_s, agent_s, layer_size, image_shape, agent_shape, action_size, is_training, scope=None):
        self.image_shape = image_shape
        self.agent_shape = agent_shape
        self.action_size = action_size
        self.layer_size = layer_size
        self.act_fn = tf.nn.leaky_relu
        self.initializer = tf.keras.initializers.he_normal()
        self.dropout_rate = 0.3
        self.is_training = is_training

        with tf.variable_scope(scope, reuse=tf.AUTO_REUSE):
            merged_image = self.ImageBlock(image_s, 19)
            merge_all = tf.concat([merged_image, agent_s], 1)
            self.value, self.policy = self.FNNBlock(merge_all)

    def MergeBlock(self, placeholders):
        lst = []
        for ph in placeholders:
            lst.append(self.ImageBlock(ph))
        image = tf.concat(lst, 1)
        return image

    def ImageBlock(self, layer_in, layer_num):
        layer_in = tf.layers.dropout(layer_in, self.dropout_rate, training=self.is_training)
        highway = tf.layers.conv2d(layer_in, self.layer_size, 5, (1, 1),
                                   kernel_initializer=self.initializer, activation=self.act_fn, padding='valid')
        highway = tf.layers.max_pooling2d(highway, 2, 2)
        highway = self.highway_layer(highway, filters=self.layer_size, kernel_size=5, strides=(1, 1), padding='same')
        outputs = tf.layers.average_pooling2d(highway, pool_size=(highway.shape[1], highway.shape[2]), strides=(1, 1))
        outputs = tf.layers.flatten(outputs)
        return outputs

    def FNNBlock(self, layer_in):
        bridge = tf.layers.dense(layer_in, units=self.layer_size,
                                 activation=self.act_fn, kernel_initializer=self.initializer)
        value = tf.layers.dense(bridge, units=1, activation=None)
        policy = tf.layers.dense(bridge, units=self.action_size, activation=None)
        policy = tf.distributions.Categorical(probs=tf.nn.softmax(policy))
        return value, policy

    def highway_layer(self, layer_in, filters, kernel_size, strides,
                      padding='same', carry_bias=-1.0, transform_bias=0.1):
        H = tf.layers.conv2d(layer_in, filters, kernel_size, strides, padding, kernel_initializer=self.initializer,
                             bias_initializer=tf.initializers.constant(carry_bias), activation=self.act_fn)
        T = tf.layers.conv2d(layer_in, filters, kernel_size, strides, padding, kernel_initializer=self.initializer,
                             bias_initializer=tf.initializers.constant(transform_bias), activation=tf.nn.sigmoid)
        C = tf.subtract(1.0, T)
        return tf.add(tf.multiply(H, T), tf.multiply(layer_in, C))
