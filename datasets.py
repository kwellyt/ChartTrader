import tensorflow as tf
from util import index_loading, image_loading, splitter


class Dataset:
    def __init__(self, features, years):
        self.features = features
        self.next_element = []
        self.dataset_init_op = []

        # setting
        for f in features:
            splitter(self.next_element, self.dataset_init_op, image_loading(f, years))
        splitter(self.next_element, self.dataset_init_op, index_loading(years))
        return

    def initialize(self):
        sess = tf.get_default_session()
        return sess.run(self.dataset_init_op)

    def next(self):
        sess = tf.get_default_session()
        date = sess.run(self.next_element[-1])[0].decode('utf-8')
        state = dict()
        for i, n in enumerate(self.features):
            state[n] = sess.run(self.next_element[i])
        return date, state
