import talib


def IKH(dfm):
    close_prices = dfm['close']

    # Tenkan-sen
    nine_period_high = dfm['high'].rolling(window=9).max()
    nine_period_low = dfm['low'].rolling(window=9).min()
    dfm['IKH_TENKAN_SEN'] = (nine_period_high + nine_period_low) / 2

    # Kijun-sen (Base Line): (26-period high + 26-period low)/2))
    period26_high = dfm['high'].rolling(window=26).max()
    period26_low = dfm['low'].rolling(window=26).min()
    dfm['IKH_KIJUN_SEN'] = (period26_high + period26_low) / 2

    # Senkou Span A (Leading Span A): (Conversion Line + Base Line)/2))
    dfm['IKH_SENKOU_SPAN_A'] = ((dfm['IKH_TENKAN_SEN'] + dfm['IKH_KIJUN_SEN']) / 2).shift(26)

    # Senkou Span B (Leading Span B): (52-period high + 52-period low)/2))
    period52_high = dfm['high'].rolling(window=52).max()
    period52_low = dfm['low'].rolling(window=52).min()
    dfm['IKH_SENKOU_SPAN_B'] = ((period52_high + period52_low) / 2).shift(26)

    # Chikou_span
    dfm['IKH_CHIKOU_SPAN'] = close_prices.shift(-26)
    return dfm


def ADX_with_DI(dfm):
    dfm['ADX'] = talib.ADX(dfm['high'], dfm['low'], dfm['close'], timeperiod=14)
    dfm['MINUS_DI'] = talib.MINUS_DI(dfm['high'], dfm['low'], dfm['close'], timeperiod=14)
    dfm['PLUS_DI'] = talib.PLUS_DI(dfm['high'], dfm['low'], dfm['close'], timeperiod=14)
    return dfm


def CCI(dfm, period=14):
    dfm['CCI'] = talib.CCI(dfm['high'], dfm['low'], dfm['close'], timeperiod=14)
    return dfm


def MACD(dfm):
    macd, macdsignal, macdhist = talib.MACD(dfm['close'], fastperiod=12, slowperiod=26, signalperiod=9)
    dfm['MACD'] = macd
    dfm['MACD_SIGNAL'] = macdsignal
    dfm['MACD_HIST'] = macdhist
    return dfm


def RSI(dfm):
    dfm['RSI_9'] = talib.RSI(dfm['close'], timeperiod=9)
    dfm['RSI_14'] = talib.RSI(dfm['close'], timeperiod=14)
    return dfm


def STOCH(dfm):
    slowk, slowd = talib.STOCH(dfm['high'], dfm['low'], dfm['close'], fastk_period=5, slowk_period=3, slowd_period=3)
    dfm['STOCH_K'] = slowk
    dfm['STOCH_D'] = slowd
    return dfm


def BBANDS(dfm):
    ub60, mb60, lb60 = talib.BBANDS(dfm['close'], timeperiod=60)
    dfm['BBANDS_60_U'] = ub60
    dfm['BBANDS_60_M'] = mb60
    dfm['BBANDS_60_L'] = lb60
    return dfm


def SAR(dfm):
    dfm['SAR'] = talib.SAR(dfm['high'], dfm['low'], acceleration=0.2, maximum=0.2)
    return dfm


def SMA(dfm):
    dfm['SMA_5'] = talib.SMA(dfm['close'], timeperiod=5)
    dfm['SMA_10'] = talib.SMA(dfm['close'], timeperiod=10)
    dfm['SMA_20'] = talib.SMA(dfm['close'], timeperiod=20)
    dfm['SMA_60'] = talib.SMA(dfm['close'], timeperiod=60)
    dfm['SMA_120'] = talib.SMA(dfm['close'], timeperiod=120)
    return dfm


def ATR(dfm):
    dfm['ATR'] = talib.ATR(dfm['high'], dfm['low'], dfm['close'], timeperiod=14)
    return dfm


def OBV(dfm):
    dfm['OBV'] = talib.OBV(dfm['close'], dfm['volume'])
    return dfm


def ROC(dfm):
    dfm['ROC_H'] = talib.ROC(dfm['high'], timeperiod=1)
    dfm['ROC_L'] = talib.ROC(dfm['low'], timeperiod=1)
    dfm['ROC_O'] = talib.ROC(dfm['open'], timeperiod=1)
    dfm['ROC_C'] = talib.ROC(dfm['close'], timeperiod=1)
    dfm['ROC_V'] = talib.ROC(dfm['volume'], timeperiod=1)
    return dfm


def VMA(dfm):
    dfm['VMA_5'] = dfm['volume'] / talib.SMA(dfm['volume'], timeperiod=5)
    dfm['VMA_20'] = dfm['volume'] / talib.SMA(dfm['volume'], timeperiod=20)
    dfm['VMA_60'] = dfm['volume'] / talib.SMA(dfm['volume'], timeperiod=60)
    dfm['VMA_120'] = dfm['volume'] / talib.SMA(dfm['volume'], timeperiod=120)
    return dfm
