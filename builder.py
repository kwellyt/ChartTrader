import tensorflow as tf
from networks import Networks


def build(cnn_size, action_size, lr, i_shape, a_shape,
          epsilon=0.2, grad_clip=5.0, value_factor=0.5, entropy_factor=0.01, scope='ppo', reuse=tf.AUTO_REUSE):
    with tf.variable_scope(scope, reuse=reuse):
        agent_s = tf.placeholder(tf.float32, [None, a_shape], name='agent_state')
        image_s = tf.placeholder(tf.float32, [None, i_shape[0], i_shape[1], i_shape[2]], name='image_state')
        returns_ph = tf.placeholder(tf.float32, [None], name='returns')
        advantages_ph = tf.placeholder(tf.float32, [None], name='advantage')
        actions_ph = tf.placeholder(tf.int32, [None], name='action')
        old_log_probs_ph = tf.placeholder(tf.float32, [None], name='old_log_prob')
        is_training = tf.placeholder(tf.bool, None, name='is_training')

        graph = Networks(image_s, agent_s, cnn_size, i_shape, a_shape, action_size, is_training, 'model')
        policy = graph.policy
        value = graph.value

        train_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope)
        advantages = tf.reshape(advantages_ph, [-1, 1])
        returns = tf.reshape(returns_ph, [-1, 1])

        with tf.variable_scope('value_loss'):
            value_loss = tf.reduce_mean(tf.square(returns - value))
            value_loss *= value_factor

        with tf.variable_scope('entropy_bonus'):
            entropy = tf.reduce_mean(policy.entropy())
            entropy *= entropy_factor

        with tf.variable_scope('policy_loss'):
            log_prob = policy.log_prob(actions_ph)
            ratio = tf.reshape(tf.exp(log_prob - old_log_probs_ph), [-1, 1])
            surr1 = ratio * advantages
            surr2 = tf.clip_by_value(ratio, 1.0 - epsilon, 1.0 + epsilon) * advantages
            surr = tf.minimum(surr1, surr2)
            policy_loss = tf.reduce_mean(surr)
        loss = value_loss - policy_loss - entropy

        with tf.variable_scope('optimization'):
            gradients = tf.gradients(loss, train_vars)
            clipped_gradients, _ = tf.clip_by_global_norm(gradients, grad_clip)
            grads_and_vars = zip(gradients, train_vars)
            global_step = tf.Variable(0, trainable=False)
            learning_rate = tf.train.exponential_decay(lr, global_step, 100000, 0.95)
            optimizer = tf.train.AdamOptimizer(learning_rate, epsilon=1e-5)
            train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)

        action = policy.sample(1)[0]
        log_policy = policy.log_prob(action)

        def train(agent, image, actions, returns, advantages, log_probs):
            feed_dict = {agent_s: agent,
                         image_s: image,
                         actions_ph: actions,
                         returns_ph: returns,
                         advantages_ph: advantages,
                         old_log_probs_ph: log_probs,
                         is_training: True}
            sess = tf.get_default_session()
            return sess.run([loss, train_op], feed_dict=feed_dict)

        def act(agent, image, training=True):
            feed_dict = {agent_s: agent,
                         image_s: image,
                         is_training: training}
            sess = tf.get_default_session()
            return sess.run([action, log_policy, value], feed_dict=feed_dict)

        def show(agent, image, training=True):
            feed_dict = {agent_s: agent,
                         image_s: image,
                         is_training: training}
            sess = tf.get_default_session()
            grp = [graph.layer1, graph.highway1_a, graph.highway1_b, graph.pooling1, graph.dropout1,
                   graph.layer2, graph.highway2_a, graph.highway2_b, graph.pooling2, graph.dropout2,
                   graph.layer3, graph.highway3_a, graph.highway3_b, graph.pooling3, graph.dropout3]
            return sess.run(grp, feed_dict=feed_dict)
    return act, train, show
