from agent import Agent
from environment import Market
import argparse
import os
import pandas as pd
import tensorflow as tf


def test():
    parser = argparse.ArgumentParser()
    parser.add_argument('--i', nargs='+', type=int, default=['SMA'], help='Technical index')
    parser.add_argument('--y', nargs='+', type=int, default=['2018'], help='The period of used data')
    parser.add_argument('--c', type=int, default=32, help='CNN feature map size')
    parser.add_argument('--h', type=int, default=128, help='Time horizon')
    args = parser.parse_args()

    folder_path = '%s%s/' % ('_'.join(args.i), args.c)
    model_path = os.path.join('./models/', folder_path)
    print(model_path)
    log_path = './logs/'

    # test
    with tf.Session() as sess:
        agent = Agent(args.c)
        envs = Market(args.i, args.y, args.h)
        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint(model_path))

        date, state = envs.reset()
        reward = 0
        start_bh = envs.recent_price
        flag = False
        buy_signal = True
        sell_signal = False
        A_cnt = {0: 0,
                 1: 0,
                 2: 0,
                 'reward': 0}
        history = {'price': [],
                   'action': [],
                   'profit': []}

        while True:
            action, volume = agent.act(state['agent'], state[args.i[0]], reward, flag,
                                       buy_signal, sell_signal, training=False)
            A_cnt[action] += 1
            A_cnt['reward'] += reward

            history['price'].append(envs.recent_price)
            history['action'].append(action)
            history['profit'].append(envs.profit)

            try:
                date, state, reward, flag, buy_signal, sell_signal = envs.step(action, volume, agent.timestep)
            except tf.errors.OutOfRangeError:
                break

            bh = (envs.recent_price - start_bh) / start_bh
            arg = (A_cnt[1], A_cnt[2], A_cnt[0], envs.profit, bh, volume)
            print('[ B: %04d S: %04d H: %04d TRADER: %.5f B&H: %.5f %.3f ]     ' % arg, end='\r')

        history = pd.DataFrame(history)
        history.to_csv('./logs/%s.csv' % '_'.join(args.i), index=False)
    return print('\n')


if __name__ == '__main__':
    test()
