from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from mpl_finance import candlestick_ohlc


def SMA_drawing(data):
    fig = Figure(figsize=(0.5, 0.5), frameon=False)
    canvas = FigureCanvas(fig)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.set_facecolor('black')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.yaxis.label.set_visible(False)
    ax.xaxis.label.set_visible(False)
    ax.autoscale(axis='x', tight=True)
    q = data[['date', 'open', 'high', 'low', 'close']]
    candlestick_ohlc(ax, q.values, width=0.2, colorup='r', colordown='b')
    var = ['SMA_5', 'SMA_10', 'SMA_20']
    data[var].plot(x=data['date'], ax=ax, legend=False, linewidth=1, alpha=0.6)
    return canvas


def BBANDS_drawing(data):
    fig = Figure(figsize=(0.5, 0.5), frameon=False)
    canvas = FigureCanvas(fig)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.set_facecolor('black')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.yaxis.label.set_visible(False)
    ax.xaxis.label.set_visible(False)
    ax.autoscale(axis='x', tight=True)
    q = data[['date', 'open', 'high', 'low', 'close']]
    candlestick_ohlc(ax, q.values, width=0.2, colorup='r', colordown='b')
    var = ['BBANDS_60_U', 'BBANDS_60_M', 'BBANDS_60_L']
    data[var].plot(x=data['date'], ax=ax, legend=False, linewidth=1, alpha=0.6)
    return canvas


def MACD_drawing(data):
    fig = Figure(figsize=(0.5, 0.5), frameon=False)
    canvas = FigureCanvas(fig)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.set_facecolor('black')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.yaxis.label.set_visible(False)
    ax.xaxis.label.set_visible(False)
    ax.autoscale(axis='x', tight=True)
    var = ['MACD', 'MACD_SIGNAL']
    data[var].plot(x=data['date'], ax=ax, legend=False, linewidth=1, alpha=0.6)
    if sum(data['MACD_HIST'] > 0) != 0:
        plus_macd = data.loc[data['MACD_HIST'] > 0, 'MACD_HIST']
        plus_macd = plus_macd[data.index]
        plus_macd = plus_macd.fillna(0)
        ax.bar(x=data['date'], height=plus_macd, color='r')
    if sum(data['MACD_HIST'] < 0) != 0:
        minus_macd = data.loc[data['MACD_HIST'] < 0, 'MACD_HIST']
        minus_macd = minus_macd[data.index]
        minus_macd = minus_macd.fillna(0)
        ax.bar(x=data['date'], height=minus_macd, color='b')
    return canvas


def RSI_drawing(data):
    fig = Figure(figsize=(0.9, 0.6), frameon=False)
    canvas = FigureCanvas(fig)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.set_facecolor('black')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.yaxis.label.set_visible(False)
    ax.xaxis.label.set_visible(False)
    ax.autoscale(axis='x', tight=True)
    start = data['date'].min()
    end = data['date'].max()
    ax.hlines(70, start, end, color='white', linewidth=1)
    ax.hlines(50, start, end, color='white', linewidth=1)
    ax.hlines(30, start, end, color='white', linewidth=1)
    var = ['RSI_9', 'RSI_14']
    data[var].plot(ax=ax, legend=False, linewidth=1.5, alpha=0.7)
    return canvas


def STOCH_drawing(data):
    fig = Figure(figsize=(0.9, 0.6), frameon=False)
    canvas = FigureCanvas(fig)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.set_facecolor('black')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.yaxis.label.set_visible(False)
    ax.xaxis.label.set_visible(False)
    ax.autoscale(axis='x', tight=True)
    start = data['date'].min()
    end = data['date'].max()
    ax.hlines(80, start, end, color='white', linewidth=1)
    ax.hlines(20, start, end, color='white', linewidth=1)
    var = ["STOCH_K", "STOCH_D"]
    data[var].plot(ax=ax, legend=False, linewidth=1.5, alpha=0.7)
    return canvas


def CCI_drawing(data):
    fig = Figure(figsize=(0.9, 0.6), frameon=False)
    canvas = FigureCanvas(fig)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.set_facecolor('black')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.yaxis.label.set_visible(False)
    ax.xaxis.label.set_visible(False)
    ax.autoscale(axis='x', tight=True)
    start = data['date'].min()
    end = data['date'].max()
    ax.hlines(100, start, end, color='white', linewidth=1)
    ax.hlines(0, start, end, color='white', linewidth=1)
    ax.hlines(-100, start, end, color='white', linewidth=1)
    var = ["CCI"]
    data[var].plot(ax=ax, legend=False, linewidth=1.5, alpha=0.7)
    return canvas


def DMI_drawing(data):
    fig = Figure(figsize=(0.9, 0.6), frameon=False)
    canvas = FigureCanvas(fig)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.set_facecolor('black')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.yaxis.label.set_visible(False)
    ax.xaxis.label.set_visible(False)
    ax.autoscale(axis='x', tight=True)
    var = ["ADX", 'MINUS_DI', 'PLUS_DI']
    data[var].plot(ax=ax, legend=False, linewidth=1.5, alpha=0.7)
    return canvas


def SAR_drawing(data):
    fig = Figure(figsize=(0.9, 0.6), frameon=False)
    canvas = FigureCanvas(fig)
    ax = fig.add_axes([0, 0, 1, 1])
    ax.set_facecolor('black')
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.get_xaxis().set_ticks([])
    ax.get_yaxis().set_ticks([])
    ax.yaxis.label.set_visible(False)
    ax.xaxis.label.set_visible(False)
    ax.autoscale(axis='x', tight=True)
    q = data[['date', 'open', 'high', 'low', 'close']]
    candlestick_ohlc(ax, q.values, width=0.5, colorup='r', colordown='b')
    data.plot.scatter(x='date', y='SAR', ax=ax, legend=False, c='white', s=0.1)
    return canvas
