from training import train


if __name__ == '__main__':
    params = {'period': 60,
              'n_index': 23,
              'n_cell': 10,
              'lr': 0.01,
              'dropout_rate': 0.7,
              'total_episodes': 100,
              'max_limit': 60}
    logdir = './log/log.txt'
    f = open(logdir, 'w')
    f.close()
    train(params, logdir)
