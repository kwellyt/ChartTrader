import tensorflow as tf
import numpy as np
import os
from modeling import onlyCNNs, onlyRNNs, allWrapper
from environment import market


def discount_rewards(r):
    gamma = 0.9
    discounted_r = gamma * r
    return discounted_r


def action_decision(action, state):
    if action == 0:
        if state:
            return 0
        else:
            return 2
    elif action == 1:
        if state:
            return 2
        else:
            return 1
    else:
        return 2


def pct_change(buy, sell):
    pct = (sell - buy) / buy
    return pct


def path_check(path):
    if os.path.exists(path) is False:
        os.mkdir(path)
    return


def train(params, logdir):
    # initialization
    agent_idx = onlyRNNs(period=params['period'],
                         n_index=params['n_index'],
                         n_cell=params['n_cell'],
                         dropout_rate=params['dropout_rate'])
    agent_bbands = onlyCNNs(type='bbands', dropout_rate=params['dropout_rate'])
    agent_ikh = onlyCNNs(type='ikh', dropout_rate=params['dropout_rate'])
    agent_macd = onlyCNNs(type='macd', dropout_rate=params['dropout_rate'])
    agent_sar = onlyCNNs(type='sar', dropout_rate=params['dropout_rate'])
    agent_sma = onlyCNNs(type='sma', dropout_rate=params['dropout_rate'])
    agent_dic = {'idx': agent_idx,
                 'bbands': agent_bbands,
                 'ikh': agent_ikh,
                 'macd': agent_macd,
                 'sar': agent_sar,
                 'sma': agent_sma}
    agent = allWrapper(agent_dic, learning_rate=params['lr'])

    # tensorboard
    log_path = './log/total'
    path_check(log_path)
    reward = tf.placeholder(tf.float32)
    profit = tf.placeholder(tf.float32)
    reward_hist = tf.summary.scalar('reward', reward)
    profit_hist = tf.summary.scalar('profit', profit)
    merged_hist = tf.summary.merge_all()

    # Session
    with tf.Session() as sess:
        # initialization
        writer = tf.summary.FileWriter(log_path, sess.graph)
        init = tf.global_variables_initializer()
        sess.run(init)

        # environment
        i = 0
        count = 0
        envs = market(sess, 'train')
        gradBuffer = sess.run(tf.trainable_variables())
        for ix, grad in enumerate(gradBuffer):
            gradBuffer[ix] = grad * 0

        while i < params['total_episodes']:
            d, s = envs._reset()
            running_reward = 0
            limit = 0
            ep_history = {'date': [],
                          'idx': [],
                          'bbands': [],
                          'ikh': [],
                          'macd': [],
                          'sar': [],
                          'sma': [],
                          'action': [],
                          'reward': []}

            while True:
                try:
                    # decision
                    feed_dict = {agent_idx.envs: s['array'],
                                 agent_bbands.images: s['bbands'],
                                 agent_ikh.images: s['ikh'],
                                 agent_macd.images: s['macd'],
                                 agent_sar.images: s['sar'],
                                 agent_sma.images: s['sma']}
                    a_dist = sess.run(tf.nn.softmax(agent.ensemble), feed_dict=feed_dict)
                    a = np.argmax(a_dist)
                    if np.random.uniform(size=1) < 0.05:
                        a = np.random.choice([0, 1], size=1)
                    a = action_decision(a, envs.STATE)

                    # action
                    ep_history['date'].append(d)
                    ep_history['idx'].append(s['array'])
                    ep_history['bbands'].append(s['bbands'])
                    ep_history['ikh'].append(s['ikh'])
                    ep_history['macd'].append(s['macd'])
                    ep_history['sar'].append(s['sar'])
                    ep_history['sma'].append(s['sma'])
                    ep_history['action'].append(a)
                    d, s1, r, flag = envs._step(a)
                    s = s1
                    limit += 1
                    
                    if limit == params['max_limit']:
                        flag = True
                        limit = 0
                    
                    # study
                    if flag:
                        # gradient add
                        pct = pct_change(envs.BUY_PRICE, envs.SELL_PRICE)
                        running_reward = discount_rewards(running_reward)
                        running_reward *= (1 + envs.PROFIT)
                        running_reward += r * (1 + pct)
                        ep_history['reward'] = [running_reward] * len(ep_history['date'])
                        feed_dict = {agent.reward_holder: np.array(ep_history['reward']),
                                     agent.action_holder: np.array(ep_history['action']),
                                     agent_idx.envs: np.vstack(ep_history['idx']),
                                     agent_bbands.images: np.vstack(ep_history['bbands']),
                                     agent_ikh.images: np.vstack(ep_history['ikh']),
                                     agent_macd.images: np.vstack(ep_history['macd']),
                                     agent_sar.images: np.vstack(ep_history['sar']),
                                     agent_sma.images: np.vstack(ep_history['sma'])}
                        grads = sess.run(agent.gradients, feed_dict=feed_dict)
                        for idx, grad in enumerate(grads):
                            gradBuffer[idx] += grad

                        # batch update
                        feed_dict = dict(zip(agent.gradient_holders, gradBuffer))
                        _ = sess.run(agent.update_batch, feed_dict=feed_dict)

                        # initialization
                        for ix, grad in enumerate(gradBuffer):
                            gradBuffer[ix] = grad = 0

                        ep_history = {'date': [],
                                      'idx': [],
                                      'bbands': [],
                                      'ikh': [],
                                      'macd': [],
                                      'sar': [],
                                      'sma': [],
                                      'action': [],
                                      'reward': []}

                        # logging
                        summary = sess.run(merged_hist, feed_dict={reward: running_reward,
                                                                   profit: envs.PROFIT})
                        writer.add_summary(summary, count)
                        count += 1
                        info = '%d DATE: %s BUY: %.4f SELL: %.4f REWARD: %.3f PROFIT: %.5f' % (count,
                                                                                               envs.SELL_DATE,
                                                                                               envs.BUY_PRICE,
                                                                                               envs.SELL_PRICE,
                                                                                               running_reward,
                                                                                               envs.PROFIT)
                        print(info)
                        with open(logdir, 'a') as f:
                            f.write(info + '\n')

                except tf.errors.OutOfRangeError:
                    break

            i += 1
