import numpy as np
from datasets import Dataset
from util import market_loading


class Market(object):
    def __init__(self, features, years, period):
        self.dataset = Dataset(features, years)
        self.date, self.price = market_loading(years)
        self.TRADING_PERIOD = period
        return

    def step(self, action, volume, timestep):
        if action == 1:  # BUY
            self._buy(volume)
        elif action == 2:  # SELL
            self._sell(volume)

        buy_signal, sell_signal = self._find_signal()

        # update
        self.sharpe.append(self.portfolio)
        self.portfolio = self.asset + self.share * self.recent_price
        self.profit = (self.portfolio - self.SEEDMONEY) / self.SEEDMONEY
        self._update_A(action)
        date, state = self.dataset.next()
        state = self._get_A(state)
        self.recent_date = date
        self.recent_price = self.price[self.date.index(self.recent_date)]

        if timestep > 0 and timestep % self.TRADING_PERIOD == 0:
            flag = True
            reward = self._reward()
            self.sharpe = []
        else:
            flag = False
            reward = 0

        return date, state, reward, flag, buy_signal, sell_signal

    def reset(self):
        self.SEEDMONEY = 10000000000               # asset at start time
        self.INTEREST = 1/1000000                  # buy/sell interest rate
        self.share = 0
        self.profit = 0
        self.asset = self.SEEDMONEY
        self.portfolio = self.asset
        self.profit_state = 0
        self.share_state = 0

        self.action_state = np.array([0, 0, 0], dtype=np.float32)
        self.dataset.initialize()
        date, state = self.dataset.next()
        state = self._get_A(state)
        self.recent_date = date
        self.recent_price = self.price[self.date.index(self.recent_date)]
        self.sharpe = []
        return date, state

    def _reward(self):
        z = self.portfolio > self.sharpe[0]
        reward = 1 if z else -1
        return reward

    def _get_A(self, state):
        s = np.array([self.profit_state, self.share_state], dtype=np.float32)
        s = np.hstack((s, self.action_state))
        s = s.reshape((-1, 5))
        state['agent'] = s
        return state

    def _update_A(self, action):
        self.profit_state = self.profit
        self.share_state = self.share / round(self.portfolio / self.recent_price, 0)
        self.action_state[0] = 0.0
        self.action_state[1] = 0.0
        self.action_state[2] = 0.0
        self.action_state[action] = 1.0
        return

    def _buy(self, volume):
        vol = self.asset * (1 - self.INTEREST) / self.recent_price
        vol = max(1, int(vol * volume))
        self.asset -= (vol * self.recent_price) * (1 + self.INTEREST)
        self.share += vol
        return

    def _sell(self, volume):
        vol = max(1, int(self.share * volume))
        self.asset += (vol * self.recent_price) * (1 - self.INTEREST)
        self.share -= vol
        return

    def _find_signal(self):
        if self.asset > self.recent_price * (1 + self.INTEREST):
            buy_signal = True
        else:
            buy_signal = False

        if self.share >= 1:
            sell_signal = True
        else:
            sell_signal = False
        return buy_signal, sell_signal
