import tensorflow as tf
import numpy as np
import pandas as pd
import os


def get_ts_array(data, period=240):
    col_num = data.shape[1]
    data_num = len(data)
    sample_num = data_num - period + 1

    ts_array = np.zeros((sample_num, period, col_num), dtype=np.float32)

    for i in range(ts_array.shape[0]):
        ts_array[i] = data[i:(i + period)]

    return ts_array


def get_data(year):
    data = pd.read_pickle('./data/numeric/data_with_numeric_index.pkl')
    date = [i for i in data.index if i[:4] == year]
    data = data.loc[date].values
    return data, date


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def write_tfrecords(ts_arr, time, year, period=60, split_index=1024):
    write = None
    sess = None

    type_path = os.path.join('data', 'numeric', year)
    if not os.path.isdir(type_path):
            os.mkdir(type_path)

    for current_index in range(len(ts_arr) - period + 1):
        # restart writer and session
        if current_index % split_index == 0:
            if write:
                try:
                    write.close()
                except Exception:
                    print('writing error!')
                    continue

            if sess:
                sess.close()

            tf.reset_default_graph()
            graph = tf.get_default_graph()
            sess = tf.Session(graph=graph)
            sess.run(tf.global_variables_initializer())

            recode_filename = './data/numeric/%s/%06d.tfrecord' % (year, current_index)

            writer = tf.python_io.TFRecordWriter(recode_filename)
            print(recode_filename)

        try:
            ts = get_ts_array(ts_arr[current_index:(current_index + period), :], period=period)
            date = time[current_index:(current_index + period)][-1]
            feature = {'date': _bytes_feature(tf.compat.as_bytes(date)),
                       'array': _bytes_feature(tf.compat.as_bytes(ts.tobytes()))}

            example = tf.train.Example(features=tf.train.Features(feature=feature))
            writer.write(example.SerializeToString())
            current_index += 1
        except Exception:
            print('processing error!')
            continue
    sess.close()


if __name__ == '__main__':
    period = 10
    years = ['2016', '2017', '2018']
    for year in years:
        data, date = get_data(year)
        write_tfrecords(data, date, year, period=period)
