 ### Momentum Indicators
 ADX: Average Directional Movement Index  
 MINUS_DI: -DI (ADX 보조)  
 PLUS_DI: +DI (ADX 보조)  
 CCI: Commodity Channel Index  
 MACD: Moving Average Convergence/Divergence  
 RSI: Relative Strength Index  
 STOCH: slow Stocastic  
 ROC: Rate Of Change

 ### Overlap Studies
 BBANDS: Bollinger Bands  
 SAR: Parabolic SAR  
 SMA: Simple Moving Average  

 ### Volatility Indicators
 ATR: Average True Range  

 ### Volume Indicators
 OBV: On Balance Volume  

 ### Hybrid Indicators
 IKH: Ichimoku Kinko Hyo  

 ### 시각적 지표
  - 이동평균선
  - MACD
  - 볼린저 밴드
  - 파라볼릭 SAR
  - 일목균형표

 ### 수치적 지표
  - 스토캐스틱
  - RSI
  - ADX + DI
  - ROC
  - CCI
  - ATR
  - OBV
