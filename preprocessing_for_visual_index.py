import pandas as pd
import os
import matplotlib.pyplot as plt
from multiprocessing import Pool
import drawer


def get_data(year):
    data = pd.read_pickle('./data/visualization/data_with_visual_index.pkl')
    date = [i for i in data.index if i[:4] == year]
    data = data.loc[date]
    data = data.reindex()
    data['date'] = list(range(len(data)))
    return data, date


def get_image_figure(data, year, var_name, current_index):
    plotter = {'SMA': drawer.SMA_drawing,
               'SAR': drawer.SAR_drawing,
               'MACD': drawer.MACD_drawing,
               'BBANDS': drawer.BBANDS_drawing,
               'STOCH': drawer.STOCH_drawing,
               'RSI': drawer.RSI_drawing,
               'CCI': drawer.CCI_drawing,
               'DMI': drawer.DMI_drawing}
    canvas = plotter[var_name](data)
    filename = '%s_%06d.png' % (var_name, current_index)
    path = os.path.join('data', 'visualization', 'since' + year, var_name, filename)
    canvas.print_figure(path, dpi=100, facecolor='black', edgecolor=None, format='png')
    print(path)
    plt.close()


def write_images(ts_arr, var_name, year, period=240):
    type_path = os.path.join('data', 'visualization', 'since' + year)
    if not os.path.isdir(type_path):
        os.mkdir(type_path)

    var_path = os.path.join('data', 'visualization', 'since' + year, var_name)
    if not os.path.isdir(var_path):
        os.mkdir(var_path)
    for current_index in range(len(ts_arr) - period + 1):
        try:
            plot_data = ts_arr.iloc[current_index:(current_index + period)]
            get_image_figure(plot_data, year, var_name, current_index)
        except Exception as e:
            print(e)
            break


def main(var_name):
    period = 10
    years = ['2016', '2017', '2018']
    for year in years:
        data, date = get_data(year)
        write_images(data, var_name, year, period=period)
    return


def single(year):
    period = 10
    var_name = 'BBANDS'
    data, date = get_data(year)
    write_images(data, var_name, year, period=period)
    return


if __name__ == '__main__':
    var_name = ['SMA', 'MACD', 'BBANDS']
    pool = Pool(len(var_name))
    pool.map(main, var_name)


#if __name__ == '__main__':
#    pool = Pool(3)
#    years = ['2016', '2017', '2018']
#    pool.map(single, years)
