import glob
import os
import pandas as pd
import tensorflow as tf


def path_check(path):
    if os.path.exists(path) is False:
        os.mkdir(path)
    return


def decode_index(serialized_example):
    feature = {'date': tf.FixedLenFeature([], tf.string),
               'array': tf.FixedLenFeature([], tf.string)}
    features = tf.parse_single_example(serialized_example, features=feature)
    date = features['date']
#    array = tf.decode_raw(features['array'], tf.float32)
    return date


def decode_image(serialized_example):
    image_file = tf.read_file(serialized_example)
    decode_image = tf.image.decode_image(image_file, channels=3)
#    decode_image = tf.image.rgb_to_grayscale(decode_image)
    decode_image = tf.image.convert_image_dtype(decode_image, dtype=tf.float32)
    decode_image = tf.cast(decode_image, tf.float32)
    return decode_image


def index_loading(years):
    filenames = []
    for year in years:
        files = sorted(glob.glob("./data/numeric/%s/*.tfrecord" % year))
        filenames.extend(files)
    train_dataset = tf.data.TFRecordDataset(filenames)
    train_dataset = train_dataset.map(decode_index)
    train_dataset = train_dataset.batch(1)
    iterator = train_dataset.make_initializable_iterator()
    next_element = iterator.get_next()
    dataset_init_op = iterator.initializer
    return next_element, dataset_init_op


def image_loading(name, years):
    filenames = []
    for year in years:
        files = sorted(glob.glob("./data/visualization/since%s/%s/*.png" % (year, name)))
        filenames.extend(files)
    train_dataset = tf.data.Dataset.from_tensor_slices(filenames)
    train_dataset = train_dataset.map(decode_image)
    train_dataset = train_dataset.batch(1)
    iterator = train_dataset.make_initializable_iterator()
    next_element = iterator.get_next()
    dataset_init_op = iterator.initializer
    return next_element, dataset_init_op


def market_loading(years):
    market_data = pd.DataFrame()
    for year in years:
        m = pd.read_pickle('./data/market_%s.pkl' % year)
        market_data = pd.concat([market_data, m])
        date = list(market_data['date'])
        price = list(market_data['close'])
    return date, price


def splitter(lst_a, lst_b, tup_ab):
    lst_a.append(tup_ab[0])
    lst_b.append(tup_ab[1])
    return
